using System;

namespace lab_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введiть початкове значення x1min: ");
            string sx1min = Console.ReadLine();
            double x1min = double.Parse(sx1min);
            Console.WriteLine("Введiть кiнцеве значення x1max: ");
            string sx1max = Console.ReadLine();
            double x1max = double.Parse(sx1max);
            Console.WriteLine("Введiть прирiст dx1: ");
            string sdx1 = Console.ReadLine();
            double dx1 = double.Parse(sdx1);
            Console.WriteLine("Введiть початкове значення x2min: ");
            string sx2min = Console.ReadLine();
            double x2min = double.Parse(sx2min);
            Console.WriteLine("Введiть кiнцеве значення x2max: ");
            string sx2max = Console.ReadLine();
            double x2max = double.Parse(sx2max);
            Console.WriteLine("Введiть прирiст dx2: ");
            string sdx2 = Console.ReadLine();
            double dx2 = double.Parse(sdx2);

            double x1 = x1min, s_sin = 0, x2, y, p_sin;
            for (; x1 <= x1max; x1 += dx1)
            {
                x2 = x2min;
                for (; x2 <= x2max; x2 += dx2)
                {
                    p_sin = Math.Sin(x2);
                    y = Math.Pow((0.1 * x1 * Math.Sin(x2) * Math.Pow(Math.Cos(x1), 2) + 55 * x1 * x2), 0.2);
                    Console.WriteLine("x1 = {0:0.000}\t\t x2 = {1:0.000} \t\t y = {2:0.000}", x1, x2, y);
                    if (p_sin > 0 && !Double.IsNaN(p_sin)) s_sin += p_sin;
                }
            }

            Console.WriteLine("Cумa додатних синусів = {0:0.000}", s_sin);
        }
    }
}