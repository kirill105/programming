using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
     
public class Program
{
 public static void Main()
 {
  Console.WriteLine("Введiть початкове значення Xmin: ");
  string sxMin = Console.ReadLine();
  double xMin = Double.Parse(sxMin);
  
  Console.WriteLine("Введiть кінцеве значення Xmax: ");
  string sxMax = Console.ReadLine();
  double xMax = Double.Parse(sxMax);
  
  Console.WriteLine("Введiть прирiст dX: ");
  string sdx = Console.ReadLine();
  double dx = Double.Parse(sdx);
  
  double x = xMin;
  double x1 = xMin * 3; 
  double y;
  double res = 0;
  while (x <= xMax)
  {
   y = Math.Log10(x * x1 * x1) + 45 * Math.Sin(x + x1);
   Console.WriteLine("x = {0}\t\t  y = {1}", x, y);
   res += Math.Pow(y, 3);
   x += dx;
  }
  
  if (Math.Abs(x - xMax - dx) > 0.0001)
  {
   y = Math.Log10(x * x1 * x1) + 45 * Math.Sin(x + x1);
   Console.WriteLine("x = {0}\t\t  y = {1}", xMax, y);
  }
  Console.Write("result = {0:0.0000}\n",res);
 }
}
