﻿using System;

namespace Lab_3
{
    class Program
    {
        static double Function(double x)
        {
            double xxxx = x * x * x * x;
            return 0.2 * x * Math.Sin(5 * x) * Math.Cos(16 * xxxx) + 55;
        }

        delegate double Integral(double min, double max, double dx);

        static void Main(string[] args)
        {
            Integral calcIntegral = integr;

        Start:
            Console.Write("Введiть початок вiдрiзку iнтегрування a: ");
            string sa = Console.ReadLine();
            double a = double.Parse(sa);

            Console.Write("Введiть кiнець вiдрiзку iнтегрування b: ");
            string sb = Console.ReadLine();
            double b = double.Parse(sb);

            Console.Write("Введiть кiлькiсть дiлянок n: ");
            string sn = Console.ReadLine();
            double n = double.Parse(sn);

            double dx = (b - a) / n;
            double y1, y2, x1, x2;
            double Intgrl = 0;

            Intgrl += calcIntegral(a, b, dx);
            Console.WriteLine("Результат: {0:0.00000}", Intgrl);
            Console.Write("Повторити розрахунок?(y/n): ");
            ConsoleKeyInfo pressedKey = Console.ReadKey();
            Console.WriteLine();
            if (pressedKey.Key == ConsoleKey.Y)
            {
                Console.WriteLine();
                goto Start;
            }
        }
        static double integr(double min, double max, double dx)
        {
            double Intgrl = 0;
            for (double i = min + dx; i <= max; i += dx)
            {
                Intgrl += dx * Function(i);
            }
            return Intgrl;
        }
    }
}
